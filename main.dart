import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AppState()),
      ],
      child: MaterialApp(
        routes: Routes.route(),
      ),
    );
  }
}

class AppState extends ChangeNotifier{
}

class Routes {
  static dynamic route(){
    return {
      '/': (BuildContext context) => XScreen(),
    };
  }
  static Route onGenerateRoute(RouteSettings? settings){
    final List<String>? pathElements = settings?.name?.split('/');
    switch (pathElements?[1]) {
      case "/":
        return MaterialPageRoute<bool>(builder: (BuildContext context) => XScreen());
      case "LoginScreen" :
        return MaterialPageRoute<bool>(builder: (BuildContext context) => LoginScreen());
      case "RegisterScreen":
        return MaterialPageRoute(builder: (BuildContext context) => RegisterScreen());
      case "ForgetPasswordPage":
        return MaterialPageRoute(builder: (BuildContext context) => ForgetPasswordScreen());
      default:
        return onUnknownRoute(const RouteSettings(name: '/Features'));
    }
  }

  static Route onUnknownRoute(RouteSettings settings){
    return MaterialPageRoute(
        builder: (_) => Scaffold(
          body: Center(
            child: Text('${settings.name?.split('/')[1]} Comming soon..'),
          ),
        )
    );
  }
}

class ForgetPasswordScreen extends StatelessWidget{
  const ForgetPasswordScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
// TODO: implement build
    throw UnimplementedError();
  }
}

class RegisterScreen  extends StatelessWidget{
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}

class SignUp extends StatelessWidget{
  const SignUp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if(AuthState.AuthStatus == logged){
      Navigator.pop();
    }
    // TODO: implement build
    throw UnimplementedError();
  }
}

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}

class XScreen extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    throw UnimplementedError();
  }
}